package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.StudentApplication;

/**
 * 学生实验室申请Service接口
 * 
 * @author lin
 * @date 2024-04-27
 */
public interface IStudentApplicationService 
{
    /**
     * 查询学生实验室申请
     * 
     * @param applicationId 学生实验室申请主键
     * @return 学生实验室申请
     */
    public StudentApplication selectStudentApplicationByApplicationId(Long applicationId);

    /**
     * 查询学生实验室申请列表
     * 
     * @param studentApplication 学生实验室申请
     * @return 学生实验室申请集合
     */
    public List<StudentApplication> selectStudentApplicationList(StudentApplication studentApplication);

    /**
     * 新增学生实验室申请
     * 
     * @param studentApplication 学生实验室申请
     * @return 结果
     */
    public int insertStudentApplication(StudentApplication studentApplication);

    /**
     * 修改学生实验室申请
     * 
     * @param studentApplication 学生实验室申请
     * @return 结果
     */
    public int updateStudentApplication(StudentApplication studentApplication);

    /**
     * 批量删除学生实验室申请
     * 
     * @param applicationIds 需要删除的学生实验室申请主键集合
     * @return 结果
     */
    public int deleteStudentApplicationByApplicationIds(Long[] applicationIds);

    /**
     * 删除学生实验室申请信息
     * 
     * @param applicationId 学生实验室申请主键
     * @return 结果
     */
    public int deleteStudentApplicationByApplicationId(Long applicationId);
}
