package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.EquipmentRepairs;

/**
 * 实验室设备报修表Service接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface IEquipmentRepairsService 
{
    /**
     * 查询实验室设备报修表
     * 
     * @param repairId 实验室设备报修表主键
     * @return 实验室设备报修表
     */
    public EquipmentRepairs selectEquipmentRepairsByRepairId(Long repairId);

    /**
     * 查询实验室设备报修表列表
     * 
     * @param equipmentRepairs 实验室设备报修表
     * @return 实验室设备报修表集合
     */
    public List<EquipmentRepairs> selectEquipmentRepairsList(EquipmentRepairs equipmentRepairs);

    /**
     * 新增实验室设备报修表
     * 
     * @param equipmentRepairs 实验室设备报修表
     * @return 结果
     */
    public int insertEquipmentRepairs(EquipmentRepairs equipmentRepairs);

    /**
     * 修改实验室设备报修表
     * 
     * @param equipmentRepairs 实验室设备报修表
     * @return 结果
     */
    public int updateEquipmentRepairs(EquipmentRepairs equipmentRepairs);

    /**
     * 批量删除实验室设备报修表
     * 
     * @param repairIds 需要删除的实验室设备报修表主键集合
     * @return 结果
     */
    public int deleteEquipmentRepairsByRepairIds(Long[] repairIds);

    /**
     * 删除实验室设备报修表信息
     * 
     * @param repairId 实验室设备报修表主键
     * @return 结果
     */
    public int deleteEquipmentRepairsByRepairId(Long repairId);
}
