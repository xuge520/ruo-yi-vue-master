package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExperimentApplicationsMapper;
import com.ruoyi.system.domain.ExperimentApplications;
import com.ruoyi.system.service.IExperimentApplicationsService;

/**
 * 实验课申请登记表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class ExperimentApplicationsServiceImpl implements IExperimentApplicationsService 
{
    @Autowired
    private ExperimentApplicationsMapper experimentApplicationsMapper;

    /**
     * 查询实验课申请登记表
     * 
     * @param applicationId 实验课申请登记表主键
     * @return 实验课申请登记表
     */
    @Override
    public ExperimentApplications selectExperimentApplicationsByApplicationId(Long applicationId)
    {
        return experimentApplicationsMapper.selectExperimentApplicationsByApplicationId(applicationId);
    }

    /**
     * 查询实验课申请登记表列表
     * 
     * @param experimentApplications 实验课申请登记表
     * @return 实验课申请登记表
     */
    @Override
    public List<ExperimentApplications> selectExperimentApplicationsList(ExperimentApplications experimentApplications)
    {
        return experimentApplicationsMapper.selectExperimentApplicationsList(experimentApplications);
    }

    /**
     * 新增实验课申请登记表
     * 
     * @param experimentApplications 实验课申请登记表
     * @return 结果
     */
    @Override
    public int insertExperimentApplications(ExperimentApplications experimentApplications)
    {
        return experimentApplicationsMapper.insertExperimentApplications(experimentApplications);
    }

    /**
     * 修改实验课申请登记表
     * 
     * @param experimentApplications 实验课申请登记表
     * @return 结果
     */
    @Override
    public int updateExperimentApplications(ExperimentApplications experimentApplications)
    {
        return experimentApplicationsMapper.updateExperimentApplications(experimentApplications);
    }

    /**
     * 批量删除实验课申请登记表
     * 
     * @param applicationIds 需要删除的实验课申请登记表主键
     * @return 结果
     */
    @Override
    public int deleteExperimentApplicationsByApplicationIds(Long[] applicationIds)
    {
        return experimentApplicationsMapper.deleteExperimentApplicationsByApplicationIds(applicationIds);
    }

    /**
     * 删除实验课申请登记表信息
     * 
     * @param applicationId 实验课申请登记表主键
     * @return 结果
     */
    @Override
    public int deleteExperimentApplicationsByApplicationId(Long applicationId)
    {
        return experimentApplicationsMapper.deleteExperimentApplicationsByApplicationId(applicationId);
    }
}
