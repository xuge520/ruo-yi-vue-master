package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.LabSchedule;

/**
 * 实验室排课Service接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface ILabScheduleService 
{
    /**
     * 查询实验室排课
     * 
     * @param id 实验室排课主键
     * @return 实验室排课
     */
    public LabSchedule selectLabScheduleById(Long id);

    /**
     * 查询实验室排课列表
     * 
     * @param labSchedule 实验室排课
     * @return 实验室排课集合
     */
    public List<LabSchedule> selectLabScheduleList(LabSchedule labSchedule);

    /**
     * 新增实验室排课
     * 
     * @param labSchedule 实验室排课
     * @return 结果
     */
    public int insertLabSchedule(LabSchedule labSchedule);

    /**
     * 修改实验室排课
     * 
     * @param labSchedule 实验室排课
     * @return 结果
     */
    public int updateLabSchedule(LabSchedule labSchedule);

    /**
     * 批量删除实验室排课
     * 
     * @param ids 需要删除的实验室排课主键集合
     * @return 结果
     */
    public int deleteLabScheduleByIds(Long[] ids);

    /**
     * 删除实验室排课信息
     * 
     * @param id 实验室排课主键
     * @return 结果
     */
    public int deleteLabScheduleById(Long id);
}
