package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Repair;

/**
 * 设备维修Service接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface IRepairService 
{
    /**
     * 查询设备维修
     * 
     * @param id 设备维修主键
     * @return 设备维修
     */
    public Repair selectRepairById(Long id);

    /**
     * 查询设备维修列表
     * 
     * @param repair 设备维修
     * @return 设备维修集合
     */
    public List<Repair> selectRepairList(Repair repair);

    /**
     * 新增设备维修
     * 
     * @param repair 设备维修
     * @return 结果
     */
    public int insertRepair(Repair repair);

    /**
     * 修改设备维修
     * 
     * @param repair 设备维修
     * @return 结果
     */
    public int updateRepair(Repair repair);

    /**
     * 批量删除设备维修
     * 
     * @param ids 需要删除的设备维修主键集合
     * @return 结果
     */
    public int deleteRepairByIds(Long[] ids);

    /**
     * 删除设备维修信息
     * 
     * @param id 设备维修主键
     * @return 结果
     */
    public int deleteRepairById(Long id);
}
