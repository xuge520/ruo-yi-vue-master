package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EquipmentRepairsMapper;
import com.ruoyi.system.domain.EquipmentRepairs;
import com.ruoyi.system.service.IEquipmentRepairsService;

/**
 * 实验室设备报修表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class EquipmentRepairsServiceImpl implements IEquipmentRepairsService 
{
    @Autowired
    private EquipmentRepairsMapper equipmentRepairsMapper;

    /**
     * 查询实验室设备报修表
     * 
     * @param repairId 实验室设备报修表主键
     * @return 实验室设备报修表
     */
    @Override
    public EquipmentRepairs selectEquipmentRepairsByRepairId(Long repairId)
    {
        return equipmentRepairsMapper.selectEquipmentRepairsByRepairId(repairId);
    }

    /**
     * 查询实验室设备报修表列表
     * 
     * @param equipmentRepairs 实验室设备报修表
     * @return 实验室设备报修表
     */
    @Override
    public List<EquipmentRepairs> selectEquipmentRepairsList(EquipmentRepairs equipmentRepairs)
    {
        return equipmentRepairsMapper.selectEquipmentRepairsList(equipmentRepairs);
    }

    /**
     * 新增实验室设备报修表
     * 
     * @param equipmentRepairs 实验室设备报修表
     * @return 结果
     */
    @Override
    public int insertEquipmentRepairs(EquipmentRepairs equipmentRepairs)
    {
        return equipmentRepairsMapper.insertEquipmentRepairs(equipmentRepairs);
    }

    /**
     * 修改实验室设备报修表
     * 
     * @param equipmentRepairs 实验室设备报修表
     * @return 结果
     */
    @Override
    public int updateEquipmentRepairs(EquipmentRepairs equipmentRepairs)
    {
        return equipmentRepairsMapper.updateEquipmentRepairs(equipmentRepairs);
    }

    /**
     * 批量删除实验室设备报修表
     * 
     * @param repairIds 需要删除的实验室设备报修表主键
     * @return 结果
     */
    @Override
    public int deleteEquipmentRepairsByRepairIds(Long[] repairIds)
    {
        return equipmentRepairsMapper.deleteEquipmentRepairsByRepairIds(repairIds);
    }

    /**
     * 删除实验室设备报修表信息
     * 
     * @param repairId 实验室设备报修表主键
     * @return 结果
     */
    @Override
    public int deleteEquipmentRepairsByRepairId(Long repairId)
    {
        return equipmentRepairsMapper.deleteEquipmentRepairsByRepairId(repairId);
    }
}
