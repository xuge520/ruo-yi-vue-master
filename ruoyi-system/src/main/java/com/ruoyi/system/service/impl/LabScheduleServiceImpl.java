package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LabScheduleMapper;
import com.ruoyi.system.domain.LabSchedule;
import com.ruoyi.system.service.ILabScheduleService;

/**
 * 实验室排课Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class LabScheduleServiceImpl implements ILabScheduleService 
{
    @Autowired
    private LabScheduleMapper labScheduleMapper;

    /**
     * 查询实验室排课
     * 
     * @param id 实验室排课主键
     * @return 实验室排课
     */
    @Override
    public LabSchedule selectLabScheduleById(Long id)
    {
        return labScheduleMapper.selectLabScheduleById(id);
    }

    /**
     * 查询实验室排课列表
     * 
     * @param labSchedule 实验室排课
     * @return 实验室排课
     */
    @Override
    public List<LabSchedule> selectLabScheduleList(LabSchedule labSchedule)
    {
        return labScheduleMapper.selectLabScheduleList(labSchedule);
    }

    /**
     * 新增实验室排课
     * 
     * @param labSchedule 实验室排课
     * @return 结果
     */
    @Override
    public int insertLabSchedule(LabSchedule labSchedule)
    {
        return labScheduleMapper.insertLabSchedule(labSchedule);
    }

    /**
     * 修改实验室排课
     * 
     * @param labSchedule 实验室排课
     * @return 结果
     */
    @Override
    public int updateLabSchedule(LabSchedule labSchedule)
    {
        return labScheduleMapper.updateLabSchedule(labSchedule);
    }

    /**
     * 批量删除实验室排课
     * 
     * @param ids 需要删除的实验室排课主键
     * @return 结果
     */
    @Override
    public int deleteLabScheduleByIds(Long[] ids)
    {
        return labScheduleMapper.deleteLabScheduleByIds(ids);
    }

    /**
     * 删除实验室排课信息
     * 
     * @param id 实验室排课主键
     * @return 结果
     */
    @Override
    public int deleteLabScheduleById(Long id)
    {
        return labScheduleMapper.deleteLabScheduleById(id);
    }
}
