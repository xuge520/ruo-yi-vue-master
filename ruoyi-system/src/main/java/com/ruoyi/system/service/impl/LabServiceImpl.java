package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LabMapper;
import com.ruoyi.system.domain.Lab;
import com.ruoyi.system.service.ILabService;

/**
 * 实验室Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class LabServiceImpl implements ILabService 
{
    @Autowired
    private LabMapper labMapper;

    /**
     * 查询实验室
     * 
     * @param id 实验室主键
     * @return 实验室
     */
    @Override
    public Lab selectLabById(Long id)
    {
        return labMapper.selectLabById(id);
    }

    /**
     * 查询实验室列表
     * 
     * @param lab 实验室
     * @return 实验室
     */
    @Override
    public List<Lab> selectLabList(Lab lab)
    {
        return labMapper.selectLabList(lab);
    }

    /**
     * 新增实验室
     * 
     * @param lab 实验室
     * @return 结果
     */
    @Override
    public int insertLab(Lab lab)
    {
        return labMapper.insertLab(lab);
    }

    /**
     * 修改实验室
     * 
     * @param lab 实验室
     * @return 结果
     */
    @Override
    public int updateLab(Lab lab)
    {
        return labMapper.updateLab(lab);
    }

    /**
     * 批量删除实验室
     * 
     * @param ids 需要删除的实验室主键
     * @return 结果
     */
    @Override
    public int deleteLabByIds(Long[] ids)
    {
        return labMapper.deleteLabByIds(ids);
    }

    /**
     * 删除实验室信息
     * 
     * @param id 实验室主键
     * @return 结果
     */
    @Override
    public int deleteLabById(Long id)
    {
        return labMapper.deleteLabById(id);
    }
}
