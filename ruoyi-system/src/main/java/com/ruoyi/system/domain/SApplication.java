package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学生借用申请对象 s_application
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class SApplication extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 申请学期；默认当前学期 */
    @Excel(name = "申请学期；默认当前学期")
    private String semester;

    /** 申请学生姓名 */
    @Excel(name = "申请学生姓名")
    private String sName;

    /** 申请学生id */
    @Excel(name = "申请学生id")
    private Long sId;

    /** 申请周次 */
    @Excel(name = "申请周次")
    private String week;

    /** 星期几 */
    @Excel(name = "星期几")
    private Long day;

    /** 申请节次 */
    @Excel(name = "申请节次")
    private String session;

    /** 实验室编号；根据前者检查是否可用 */
    @Excel(name = "实验室编号；根据前者检查是否可用")
    private Long labId;

    /** 申请原因 */
    @Excel(name = "申请原因")
    private String reason;

    /** 填报日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "填报日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date date;

    /** 申请状态；0为未审核，1为通过，2为驳回，3为使用完毕 */
    @Excel(name = "申请状态；0为未审核，1为通过，2为驳回，3为使用完毕")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSemester(String semester) 
    {
        this.semester = semester;
    }

    public String getSemester() 
    {
        return semester;
    }
    public void setsName(String sName) 
    {
        this.sName = sName;
    }

    public String getsName() 
    {
        return sName;
    }
    public void setsId(Long sId) 
    {
        this.sId = sId;
    }

    public Long getsId() 
    {
        return sId;
    }
    public void setWeek(String week) 
    {
        this.week = week;
    }

    public String getWeek() 
    {
        return week;
    }
    public void setDay(Long day) 
    {
        this.day = day;
    }

    public Long getDay() 
    {
        return day;
    }
    public void setSession(String session) 
    {
        this.session = session;
    }

    public String getSession() 
    {
        return session;
    }
    public void setLabId(Long labId) 
    {
        this.labId = labId;
    }

    public Long getLabId() 
    {
        return labId;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }
    public void setDate(Date date) 
    {
        this.date = date;
    }

    public Date getDate() 
    {
        return date;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("semester", getSemester())
            .append("sName", getsName())
            .append("sId", getsId())
            .append("week", getWeek())
            .append("day", getDay())
            .append("session", getSession())
            .append("labId", getLabId())
            .append("reason", getReason())
            .append("date", getDate())
            .append("status", getStatus())
            .toString();
    }
}
