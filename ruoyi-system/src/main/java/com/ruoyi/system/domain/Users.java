package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户对象 users
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public class Users extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 账号，实验员与教师为工号，学生为学号。 */
    @Excel(name = "账号，实验员与教师为工号，学生为学号。")
    private String accoun;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 密码，默认“123456” */
    @Excel(name = "密码，默认“123456”")
    private String password;

    /** 职称 */
    @Excel(name = "职称")
    private String title;

    /** 专业 */
    @Excel(name = "专业")
    private String major;

    /** 班级 */
    @Excel(name = "班级")
    private String uClass;

    /** 角色；0为管理员，1为教师，2为实验员，3为学生。 */
    @Excel(name = "角色；0为管理员，1为教师，2为实验员，3为学生。")
    private Long role;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAccoun(String accoun) 
    {
        this.accoun = accoun;
    }

    public String getAccoun() 
    {
        return accoun;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setMajor(String major) 
    {
        this.major = major;
    }

    public String getMajor() 
    {
        return major;
    }
    public void setuClass(String uClass) 
    {
        this.uClass = uClass;
    }

    public String getuClass() 
    {
        return uClass;
    }
    public void setRole(Long role) 
    {
        this.role = role;
    }

    public Long getRole() 
    {
        return role;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("accoun", getAccoun())
            .append("name", getName())
            .append("password", getPassword())
            .append("title", getTitle())
            .append("major", getMajor())
            .append("uClass", getuClass())
            .append("role", getRole())
            .toString();
    }
}
