package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.EquipmentRepairs;
import com.ruoyi.system.service.IEquipmentRepairsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实验室设备报修表Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/system/repairs")
public class EquipmentRepairsController extends BaseController
{
    @Autowired
    private IEquipmentRepairsService equipmentRepairsService;

    /**
     * 查询实验室设备报修表列表
     */
    @PreAuthorize("@ss.hasPermi('system:repairs:list')")
    @GetMapping("/list")
    public TableDataInfo list(EquipmentRepairs equipmentRepairs)
    {
        startPage();
        List<EquipmentRepairs> list = equipmentRepairsService.selectEquipmentRepairsList(equipmentRepairs);
        return getDataTable(list);
    }

    /**
     * 导出实验室设备报修表列表
     */
    @PreAuthorize("@ss.hasPermi('system:repairs:export')")
    @Log(title = "实验室设备报修表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EquipmentRepairs equipmentRepairs)
    {
        List<EquipmentRepairs> list = equipmentRepairsService.selectEquipmentRepairsList(equipmentRepairs);
        ExcelUtil<EquipmentRepairs> util = new ExcelUtil<EquipmentRepairs>(EquipmentRepairs.class);
        util.exportExcel(response, list, "实验室设备报修表数据");
    }

    /**
     * 获取实验室设备报修表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:repairs:query')")
    @GetMapping(value = "/{repairId}")
    public AjaxResult getInfo(@PathVariable("repairId") Long repairId)
    {
        return success(equipmentRepairsService.selectEquipmentRepairsByRepairId(repairId));
    }

    /**
     * 新增实验室设备报修表
     */
    @PreAuthorize("@ss.hasPermi('system:repairs:add')")
    @Log(title = "实验室设备报修表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EquipmentRepairs equipmentRepairs)
    {
        return toAjax(equipmentRepairsService.insertEquipmentRepairs(equipmentRepairs));
    }

    /**
     * 修改实验室设备报修表
     */
    @PreAuthorize("@ss.hasPermi('system:repairs:edit')")
    @Log(title = "实验室设备报修表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EquipmentRepairs equipmentRepairs)
    {
        return toAjax(equipmentRepairsService.updateEquipmentRepairs(equipmentRepairs));
    }

    /**
     * 删除实验室设备报修表
     */
    @PreAuthorize("@ss.hasPermi('system:repairs:remove')")
    @Log(title = "实验室设备报修表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{repairIds}")
    public AjaxResult remove(@PathVariable Long[] repairIds)
    {
        return toAjax(equipmentRepairsService.deleteEquipmentRepairsByRepairIds(repairIds));
    }
}
