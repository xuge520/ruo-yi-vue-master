package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SApplication;
import com.ruoyi.system.service.ISApplicationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学生借用申请Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/system/s_application")
public class SApplicationController extends BaseController
{
    @Autowired
    private ISApplicationService sApplicationService;

    /**
     * 查询学生借用申请列表
     */
    @PreAuthorize("@ss.hasPermi('system:s_application:list')")
    @GetMapping("/list")
    public TableDataInfo list(SApplication sApplication)
    {
        startPage();
        List<SApplication> list = sApplicationService.selectSApplicationList(sApplication);
        return getDataTable(list);
    }

    /**
     * 导出学生借用申请列表
     */
    @PreAuthorize("@ss.hasPermi('system:s_application:export')")
    @Log(title = "学生借用申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SApplication sApplication)
    {
        List<SApplication> list = sApplicationService.selectSApplicationList(sApplication);
        ExcelUtil<SApplication> util = new ExcelUtil<SApplication>(SApplication.class);
        util.exportExcel(response, list, "学生借用申请数据");
    }

    /**
     * 获取学生借用申请详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:s_application:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sApplicationService.selectSApplicationById(id));
    }

    /**
     * 新增学生借用申请
     */
    @PreAuthorize("@ss.hasPermi('system:s_application:add')")
    @Log(title = "学生借用申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SApplication sApplication)
    {
        return toAjax(sApplicationService.insertSApplication(sApplication));
    }

    /**
     * 修改学生借用申请
     */
    @PreAuthorize("@ss.hasPermi('system:s_application:edit')")
    @Log(title = "学生借用申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SApplication sApplication)
    {
        return toAjax(sApplicationService.updateSApplication(sApplication));
    }

    /**
     * 删除学生借用申请
     */
    @PreAuthorize("@ss.hasPermi('system:s_application:remove')")
    @Log(title = "学生借用申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sApplicationService.deleteSApplicationByIds(ids));
    }
}
