package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ExperimentApplications;
import com.ruoyi.system.service.IExperimentApplicationsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实验课申请登记表Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/system/applications")
public class ExperimentApplicationsController extends BaseController
{
    @Autowired
    private IExperimentApplicationsService experimentApplicationsService;

    /**
     * 查询实验课申请登记表列表
     */
    @PreAuthorize("@ss.hasPermi('system:applications:list')")
    @GetMapping("/list")
    public TableDataInfo list(ExperimentApplications experimentApplications)
    {
        startPage();
        List<ExperimentApplications> list = experimentApplicationsService.selectExperimentApplicationsList(experimentApplications);
        return getDataTable(list);
    }

    /**
     * 导出实验课申请登记表列表
     */
    @PreAuthorize("@ss.hasPermi('system:applications:export')")
    @Log(title = "实验课申请登记表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExperimentApplications experimentApplications)
    {
        List<ExperimentApplications> list = experimentApplicationsService.selectExperimentApplicationsList(experimentApplications);
        ExcelUtil<ExperimentApplications> util = new ExcelUtil<ExperimentApplications>(ExperimentApplications.class);
        util.exportExcel(response, list, "实验课申请登记表数据");
    }

    /**
     * 获取实验课申请登记表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:applications:query')")
    @GetMapping(value = "/{applicationId}")
    public AjaxResult getInfo(@PathVariable("applicationId") Long applicationId)
    {
        return success(experimentApplicationsService.selectExperimentApplicationsByApplicationId(applicationId));
    }

    /**
     * 新增实验课申请登记表
     */
    @PreAuthorize("@ss.hasPermi('system:applications:add')")
    @Log(title = "实验课申请登记表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExperimentApplications experimentApplications)
    {
        return toAjax(experimentApplicationsService.insertExperimentApplications(experimentApplications));
    }

    /**
     * 修改实验课申请登记表
     */
    @PreAuthorize("@ss.hasPermi('system:applications:edit')")
    @Log(title = "实验课申请登记表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExperimentApplications experimentApplications)
    {
        return toAjax(experimentApplicationsService.updateExperimentApplications(experimentApplications));
    }

    /**
     * 删除实验课申请登记表
     */
    @PreAuthorize("@ss.hasPermi('system:applications:remove')")
    @Log(title = "实验课申请登记表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{applicationIds}")
    public AjaxResult remove(@PathVariable Long[] applicationIds)
    {
        return toAjax(experimentApplicationsService.deleteExperimentApplicationsByApplicationIds(applicationIds));
    }
}
