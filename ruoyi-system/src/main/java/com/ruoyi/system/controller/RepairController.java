package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Repair;
import com.ruoyi.system.service.IRepairService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 设备维修Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/system/repair")
public class RepairController extends BaseController
{
    @Autowired
    private IRepairService repairService;

    /**
     * 查询设备维修列表
     */
    @PreAuthorize("@ss.hasPermi('system:repair:list')")
    @GetMapping("/list")
    public TableDataInfo list(Repair repair)
    {
        startPage();
        List<Repair> list = repairService.selectRepairList(repair);
        return getDataTable(list);
    }

    /**
     * 导出设备维修列表
     */
    @PreAuthorize("@ss.hasPermi('system:repair:export')")
    @Log(title = "设备维修", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Repair repair)
    {
        List<Repair> list = repairService.selectRepairList(repair);
        ExcelUtil<Repair> util = new ExcelUtil<Repair>(Repair.class);
        util.exportExcel(response, list, "设备维修数据");
    }

    /**
     * 获取设备维修详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:repair:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(repairService.selectRepairById(id));
    }

    /**
     * 新增设备维修
     */
    @PreAuthorize("@ss.hasPermi('system:repair:add')")
    @Log(title = "设备维修", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Repair repair)
    {
        return toAjax(repairService.insertRepair(repair));
    }

    /**
     * 修改设备维修
     */
    @PreAuthorize("@ss.hasPermi('system:repair:edit')")
    @Log(title = "设备维修", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Repair repair)
    {
        return toAjax(repairService.updateRepair(repair));
    }

    /**
     * 删除设备维修
     */
    @PreAuthorize("@ss.hasPermi('system:repair:remove')")
    @Log(title = "设备维修", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(repairService.deleteRepairByIds(ids));
    }
}
