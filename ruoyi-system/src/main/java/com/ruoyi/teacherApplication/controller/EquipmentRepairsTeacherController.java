package com.ruoyi.teacherApplication.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.teacherApplication.domain.EquipmentRepairsTeacher;
import com.ruoyi.teacherApplication.service.IEquipmentRepairsTeacherService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实验室设备报修表Controller
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
@RestController
@RequestMapping("/teacherApplication/teacherApplication")
public class EquipmentRepairsTeacherController extends BaseController
{
    @Autowired
    private IEquipmentRepairsTeacherService equipmentRepairsTeacherService;

    /**
     * 查询实验室设备报修表列表
     */
    @PreAuthorize("@ss.hasPermi('teacherApplication:teacherApplication:list')")
    @GetMapping("/list")
    public TableDataInfo list(EquipmentRepairsTeacher equipmentRepairsTeacher)
    {
        startPage();
        List<EquipmentRepairsTeacher> list = equipmentRepairsTeacherService.selectEquipmentRepairsTeacherList(equipmentRepairsTeacher);
        return getDataTable(list);
    }

    /**
     * 导出实验室设备报修表列表
     */
    @PreAuthorize("@ss.hasPermi('teacherApplication:teacherApplication:export')")
    @Log(title = "实验室设备报修表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EquipmentRepairsTeacher equipmentRepairsTeacher)
    {
        List<EquipmentRepairsTeacher> list = equipmentRepairsTeacherService.selectEquipmentRepairsTeacherList(equipmentRepairsTeacher);
        ExcelUtil<EquipmentRepairsTeacher> util = new ExcelUtil<EquipmentRepairsTeacher>(EquipmentRepairsTeacher.class);
        util.exportExcel(response, list, "实验室设备报修表数据");
    }

    /**
     * 获取实验室设备报修表详细信息
     */
    @PreAuthorize("@ss.hasPermi('teacherApplication:teacherApplication:query')")
    @GetMapping(value = "/{repairId}")
    public AjaxResult getInfo(@PathVariable("repairId") Long repairId)
    {
        return success(equipmentRepairsTeacherService.selectEquipmentRepairsTeacherByRepairId(repairId));
    }

    /**
     * 新增实验室设备报修表
     */
    @PreAuthorize("@ss.hasPermi('teacherApplication:teacherApplication:add')")
    @Log(title = "实验室设备报修表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EquipmentRepairsTeacher equipmentRepairsTeacher)
    {
        return toAjax(equipmentRepairsTeacherService.insertEquipmentRepairsTeacher(equipmentRepairsTeacher));
    }

    /**
     * 修改实验室设备报修表
     */
    @PreAuthorize("@ss.hasPermi('teacherApplication:teacherApplication:edit')")
    @Log(title = "实验室设备报修表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EquipmentRepairsTeacher equipmentRepairsTeacher)
    {
        return toAjax(equipmentRepairsTeacherService.updateEquipmentRepairsTeacher(equipmentRepairsTeacher));
    }

    /**
     * 删除实验室设备报修表
     */
    @PreAuthorize("@ss.hasPermi('teacherApplication:teacherApplication:remove')")
    @Log(title = "实验室设备报修表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{repairIds}")
    public AjaxResult remove(@PathVariable Long[] repairIds)
    {
        return toAjax(equipmentRepairsTeacherService.deleteEquipmentRepairsTeacherByRepairIds(repairIds));
    }
}
