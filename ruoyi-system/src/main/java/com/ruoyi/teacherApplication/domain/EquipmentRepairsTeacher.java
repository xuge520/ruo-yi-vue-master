package com.ruoyi.teacherApplication.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 实验室设备报修表对象 equipment_repairs
 * 
 * @author ruoyi
 * @date 2024-04-27
 */
public class EquipmentRepairsTeacher extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 报修唯一标识符 */
    private Long repairId;

    /** 报修教师的唯一标识符 */
    @Excel(name = "报修教师的唯一标识符")
    private Long teacherId;

    /** 实验室编号 */
    @Excel(name = "实验室编号")
    private Long labId;

    /** 故障描述 */
    @Excel(name = "故障描述")
    private String issueDescription;

    /** 报修状态 (“已维修”, “未维修”, “维修中”) */
    @Excel(name = "报修状态 (“已维修”, “未维修”, “维修中”)")
    private String repairStatus;

    /** 报修日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报修日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date repairDate;

    /** 维修情况说明 */
    @Excel(name = "维修情况说明")
    private String repairIllustrate;

    public void setRepairId(Long repairId) 
    {
        this.repairId = repairId;
    }

    public Long getRepairId() 
    {
        return repairId;
    }
    public void setTeacherId(Long teacherId) 
    {
        this.teacherId = teacherId;
    }

    public Long getTeacherId() 
    {
        return teacherId;
    }
    public void setLabId(Long labId) 
    {
        this.labId = labId;
    }

    public Long getLabId() 
    {
        return labId;
    }
    public void setIssueDescription(String issueDescription) 
    {
        this.issueDescription = issueDescription;
    }

    public String getIssueDescription() 
    {
        return issueDescription;
    }
    public void setRepairStatus(String repairStatus) 
    {
        this.repairStatus = repairStatus;
    }

    public String getRepairStatus() 
    {
        return repairStatus;
    }
    public void setRepairDate(Date repairDate) 
    {
        this.repairDate = repairDate;
    }

    public Date getRepairDate() 
    {
        return repairDate;
    }
    public void setRepairIllustrate(String repairIllustrate) 
    {
        this.repairIllustrate = repairIllustrate;
    }

    public String getRepairIllustrate() 
    {
        return repairIllustrate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("repairId", getRepairId())
            .append("teacherId", getTeacherId())
            .append("labId", getLabId())
            .append("issueDescription", getIssueDescription())
            .append("repairStatus", getRepairStatus())
            .append("repairDate", getRepairDate())
            .append("repairIllustrate", getRepairIllustrate())
            .toString();
    }
}
