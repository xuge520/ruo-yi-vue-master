-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室设备报修表', '3', '1', 'repairs', 'system/repairs/index', 1, 0, 'C', '0', '0', 'system:repairs:list', '#', 'admin', sysdate(), '', null, '实验室设备报修表菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室设备报修表查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'system:repairs:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室设备报修表新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'system:repairs:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室设备报修表修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'system:repairs:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室设备报修表删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'system:repairs:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室设备报修表导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'system:repairs:export',       '#', 'admin', sysdate(), '', null, '');