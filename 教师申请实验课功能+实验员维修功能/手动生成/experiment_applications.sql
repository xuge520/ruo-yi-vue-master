/*
Navicat MySQL Data Transfer

Source Server         : localhost_3308
Source Server Version : 80018
Source Host           : localhost:3308
Source Database       : ry-vue

Target Server Type    : MYSQL
Target Server Version : 80018
File Encoding         : 65001

Date: 2024-04-26 17:55:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for experiment_applications
-- ----------------------------
DROP TABLE IF EXISTS `experiment_applications`;
CREATE TABLE `experiment_applications` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) DEFAULT NULL,
  `term` varchar(10) DEFAULT NULL,
  `course_name` varchar(50) DEFAULT NULL,
  `lab_type` varchar(20) DEFAULT NULL,
  `class_name` varchar(50) DEFAULT NULL,
  `student_count` int(11) DEFAULT NULL,
  `start_week` int(11) DEFAULT NULL,
  `end_week` int(11) DEFAULT NULL,
  `class_sessions` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of experiment_applications
-- ----------------------------
INSERT INTO `experiment_applications` VALUES ('1', '101', '2022-2023-', '数据库设计', '软件', 'A班', '30', '1', '18', '1-2');
INSERT INTO `experiment_applications` VALUES ('2', '1', '1', '1', null, '1', '1', '1', '1', '1');
