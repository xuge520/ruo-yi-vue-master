-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师排课申请', '3', '1', 't_application', 'system/t_application/index', 1, 0, 'C', '0', '0', 'system:t_application:list', '#', 'admin', sysdate(), '', null, '教师排课申请菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师排课申请查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'system:t_application:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师排课申请新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'system:t_application:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师排课申请修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'system:t_application:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师排课申请删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'system:t_application:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('教师排课申请导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'system:t_application:export',       '#', 'admin', sysdate(), '', null, '');