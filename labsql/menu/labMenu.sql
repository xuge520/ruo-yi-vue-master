-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室', '3', '1', 'lab', 'system/lab/index', 1, 0, 'C', '0', '0', 'system:lab:list', '#', 'admin', sysdate(), '', null, '实验室菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'system:lab:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'system:lab:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'system:lab:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'system:lab:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'system:lab:export',       '#', 'admin', sysdate(), '', null, '');