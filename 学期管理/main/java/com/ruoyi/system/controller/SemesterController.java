package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Semester;
import com.ruoyi.system.service.ISemesterService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学期Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@RestController
@RequestMapping("/system/semester")
public class SemesterController extends BaseController
{
    @Autowired
    private ISemesterService semesterService;

    /**
     * 查询学期列表
     */
    @PreAuthorize("@ss.hasPermi('system:semester:list')")
    @GetMapping("/list")
    public TableDataInfo list(Semester semester)
    {
        startPage();
        List<Semester> list = semesterService.selectSemesterList(semester);
        return getDataTable(list);
    }

    /**
     * 导出学期列表
     */
    @PreAuthorize("@ss.hasPermi('system:semester:export')")
    @Log(title = "学期", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Semester semester)
    {
        List<Semester> list = semesterService.selectSemesterList(semester);
        ExcelUtil<Semester> util = new ExcelUtil<Semester>(Semester.class);
        util.exportExcel(response, list, "学期数据");
    }

    /**
     * 获取学期详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:semester:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(semesterService.selectSemesterById(id));
    }

    /**
     * 新增学期
     */
    @PreAuthorize("@ss.hasPermi('system:semester:add')")
    @Log(title = "学期", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Semester semester)
    {
        return toAjax(semesterService.insertSemester(semester));
    }

    /**
     * 修改学期
     */
    @PreAuthorize("@ss.hasPermi('system:semester:edit')")
    @Log(title = "学期", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Semester semester)
    {
        return toAjax(semesterService.updateSemester(semester));
    }

    /**
     * 删除学期
     */
    @PreAuthorize("@ss.hasPermi('system:semester:remove')")
    @Log(title = "学期", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(semesterService.deleteSemesterByIds(ids));
    }
}
