import request from '@/utils/request'

// 查询实验室列表
export function listLab(query) {
  return request({
    url: '/system/lab/list',
    method: 'get',
    params: query
  })
}

// 查询实验室详细
export function getLab(id) {
  return request({
    url: '/system/lab/' + id,
    method: 'get'
  })
}

// 新增实验室
export function addLab(data) {
  return request({
    url: '/system/lab',
    method: 'post',
    data: data
  })
}

// 修改实验室
export function updateLab(data) {
  return request({
    url: '/system/lab',
    method: 'put',
    data: data
  })
}

// 删除实验室
export function delLab(id) {
  return request({
    url: '/system/lab/' + id,
    method: 'delete'
  })
}
