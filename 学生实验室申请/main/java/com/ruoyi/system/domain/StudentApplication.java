package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学生实验室申请对象 student_application
 * 
 * @author lin
 * @date 2024-04-27
 */
public class StudentApplication extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 申请流水号 */
    private Long applicationId;

    /** 学号 */
    @Excel(name = "学号")
    private String studentId;

    /** 姓名 */
    @Excel(name = "姓名")
    private String studentName;

    /** 班级 */
    @Excel(name = "班级")
    private String studentClass;

    /** 实验室编号 */
    @Excel(name = "实验室编号")
    private String labId;

    /** 周次 */
    @Excel(name = "周次")
    private Long applicationWeek;

    /** 星期几 */
    @Excel(name = "星期几")
    private Long applicationDay;

    /** 节次 */
    @Excel(name = "节次")
    private String applicationSection;

    /** 原因 */
    @Excel(name = "原因")
    private String applicationReason;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date applicationTime;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private String applicationStatus;

    public void setApplicationId(Long applicationId) 
    {
        this.applicationId = applicationId;
    }

    public Long getApplicationId() 
    {
        return applicationId;
    }
    public void setStudentId(String studentId) 
    {
        this.studentId = studentId;
    }

    public String getStudentId() 
    {
        return studentId;
    }
    public void setStudentName(String studentName) 
    {
        this.studentName = studentName;
    }

    public String getStudentName() 
    {
        return studentName;
    }
    public void setStudentClass(String studentClass) 
    {
        this.studentClass = studentClass;
    }

    public String getStudentClass() 
    {
        return studentClass;
    }
    public void setLabId(String labId) 
    {
        this.labId = labId;
    }

    public String getLabId() 
    {
        return labId;
    }
    public void setApplicationWeek(Long applicationWeek) 
    {
        this.applicationWeek = applicationWeek;
    }

    public Long getApplicationWeek() 
    {
        return applicationWeek;
    }
    public void setApplicationDay(Long applicationDay) 
    {
        this.applicationDay = applicationDay;
    }

    public Long getApplicationDay() 
    {
        return applicationDay;
    }
    public void setApplicationSection(String applicationSection) 
    {
        this.applicationSection = applicationSection;
    }

    public String getApplicationSection() 
    {
        return applicationSection;
    }
    public void setApplicationReason(String applicationReason) 
    {
        this.applicationReason = applicationReason;
    }

    public String getApplicationReason() 
    {
        return applicationReason;
    }
    public void setApplicationTime(Date applicationTime) 
    {
        this.applicationTime = applicationTime;
    }

    public Date getApplicationTime() 
    {
        return applicationTime;
    }
    public void setApplicationStatus(String applicationStatus) 
    {
        this.applicationStatus = applicationStatus;
    }

    public String getApplicationStatus() 
    {
        return applicationStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("applicationId", getApplicationId())
            .append("studentId", getStudentId())
            .append("studentName", getStudentName())
            .append("studentClass", getStudentClass())
            .append("labId", getLabId())
            .append("applicationWeek", getApplicationWeek())
            .append("applicationDay", getApplicationDay())
            .append("applicationSection", getApplicationSection())
            .append("applicationReason", getApplicationReason())
            .append("applicationTime", getApplicationTime())
            .append("applicationStatus", getApplicationStatus())
            .toString();
    }
}
