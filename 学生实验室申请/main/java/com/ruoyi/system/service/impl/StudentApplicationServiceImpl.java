package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StudentApplicationMapper;
import com.ruoyi.system.domain.StudentApplication;
import com.ruoyi.system.service.IStudentApplicationService;

/**
 * 学生实验室申请Service业务层处理
 * 
 * @author lin
 * @date 2024-04-27
 */
@Service
public class StudentApplicationServiceImpl implements IStudentApplicationService 
{
    @Autowired
    private StudentApplicationMapper studentApplicationMapper;

    /**
     * 查询学生实验室申请
     * 
     * @param applicationId 学生实验室申请主键
     * @return 学生实验室申请
     */
    @Override
    public StudentApplication selectStudentApplicationByApplicationId(Long applicationId)
    {
        return studentApplicationMapper.selectStudentApplicationByApplicationId(applicationId);
    }

    /**
     * 查询学生实验室申请列表
     * 
     * @param studentApplication 学生实验室申请
     * @return 学生实验室申请
     */
    @Override
    public List<StudentApplication> selectStudentApplicationList(StudentApplication studentApplication)
    {
        return studentApplicationMapper.selectStudentApplicationList(studentApplication);
    }

    /**
     * 新增学生实验室申请
     * 
     * @param studentApplication 学生实验室申请
     * @return 结果
     */
    @Override
    public int insertStudentApplication(StudentApplication studentApplication)
    {
        return studentApplicationMapper.insertStudentApplication(studentApplication);
    }

    /**
     * 修改学生实验室申请
     * 
     * @param studentApplication 学生实验室申请
     * @return 结果
     */
    @Override
    public int updateStudentApplication(StudentApplication studentApplication)
    {
        return studentApplicationMapper.updateStudentApplication(studentApplication);
    }

    /**
     * 批量删除学生实验室申请
     * 
     * @param applicationIds 需要删除的学生实验室申请主键
     * @return 结果
     */
    @Override
    public int deleteStudentApplicationByApplicationIds(Long[] applicationIds)
    {
        return studentApplicationMapper.deleteStudentApplicationByApplicationIds(applicationIds);
    }

    /**
     * 删除学生实验室申请信息
     * 
     * @param applicationId 学生实验室申请主键
     * @return 结果
     */
    @Override
    public int deleteStudentApplicationByApplicationId(Long applicationId)
    {
        return studentApplicationMapper.deleteStudentApplicationByApplicationId(applicationId);
    }
}
