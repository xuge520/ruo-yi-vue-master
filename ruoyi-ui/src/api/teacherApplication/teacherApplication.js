import request from '@/utils/request'

// 查询实验室设备报修表列表
export function listTeacherApplication(query) {
  return request({
    url: '/teacherApplication/teacherApplication/list',
    method: 'get',
    params: query
  })
}

// 查询实验室设备报修表详细
export function getTeacherApplication(repairId) {
  return request({
    url: '/teacherApplication/teacherApplication/' + repairId,
    method: 'get'
  })
}

// 新增实验室设备报修表
export function addTeacherApplication(data) {
  return request({
    url: '/teacherApplication/teacherApplication',
    method: 'post',
    data: data
  })
}

// 修改实验室设备报修表
export function updateTeacherApplication(data) {
  return request({
    url: '/teacherApplication/teacherApplication',
    method: 'put',
    data: data
  })
}

// 删除实验室设备报修表
export function delTeacherApplication(repairId) {
  return request({
    url: '/teacherApplication/teacherApplication/' + repairId,
    method: 'delete'
  })
}
