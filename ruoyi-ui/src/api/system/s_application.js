import request from '@/utils/request'

// 查询学生借用申请列表
export function listS_application(query) {
  return request({
    url: '/system/s_application/list',
    method: 'get',
    params: query
  })
}

// 查询学生借用申请详细
export function getS_application(id) {
  return request({
    url: '/system/s_application/' + id,
    method: 'get'
  })
}

// 新增学生借用申请
export function addS_application(data) {
  return request({
    url: '/system/s_application',
    method: 'post',
    data: data
  })
}

// 修改学生借用申请
export function updateS_application(data) {
  return request({
    url: '/system/s_application',
    method: 'put',
    data: data
  })
}

// 删除学生借用申请
export function delS_application(id) {
  return request({
    url: '/system/s_application/' + id,
    method: 'delete'
  })
}
