import request from '@/utils/request'

// 查询实验室设备报修表列表
export function listRepairs(query) {
  return request({
    url: '/system/repairs/list',
    method: 'get',
    params: query
  })
}

// 查询实验室设备报修表详细
export function getRepairs(repairId) {
  return request({
    url: '/system/repairs/' + repairId,
    method: 'get'
  })
}

// 新增实验室设备报修表
export function addRepairs(data) {
  return request({
    url: '/system/repairs',
    method: 'post',
    data: data
  })
}

// 修改实验室设备报修表
export function updateRepairs(data) {
  return request({
    url: '/system/repairs',
    method: 'put',
    data: data
  })
}

// 删除实验室设备报修表
export function delRepairs(repairId) {
  return request({
    url: '/system/repairs/' + repairId,
    method: 'delete'
  })
}
