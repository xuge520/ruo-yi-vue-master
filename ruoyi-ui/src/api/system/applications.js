import request from '@/utils/request'

// 查询实验课申请登记表列表
export function listApplications(query) {
  return request({
    url: '/system/applications/list',
    method: 'get',
    params: query
  })
}

// 查询实验课申请登记表详细
export function getApplications(applicationId) {
  return request({
    url: '/system/applications/' + applicationId,
    method: 'get'
  })
}

// 新增实验课申请登记表
export function addApplications(data) {
  return request({
    url: '/system/applications',
    method: 'post',
    data: data
  })
}

// 修改实验课申请登记表
export function updateApplications(data) {
  return request({
    url: '/system/applications',
    method: 'put',
    data: data
  })
}

// 删除实验课申请登记表
export function delApplications(applicationId) {
  return request({
    url: '/system/applications/' + applicationId,
    method: 'delete'
  })
}
