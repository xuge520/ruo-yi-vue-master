import request from '@/utils/request'

// 查询实验室借用审批表列表
export function listLabborrowingapproval(query) {
  return request({
    url: '/system/labborrowingapproval/list',
    method: 'get',
    params: query
  })
}

// 查询实验室借用审批表详细
export function getLabborrowingapproval(applicationId) {
  return request({
    url: '/system/labborrowingapproval/' + applicationId,
    method: 'get'
  })
}

// 新增实验室借用审批表
export function addLabborrowingapproval(data) {
  return request({
    url: '/system/labborrowingapproval',
    method: 'post',
    data: data
  })
}

// 修改实验室借用审批表
export function updateLabborrowingapproval(data) {
  return request({
    url: '/system/labborrowingapproval',
    method: 'put',
    data: data
  })
}

// 删除实验室借用审批表
export function delLabborrowingapproval(applicationId) {
  return request({
    url: '/system/labborrowingapproval/' + applicationId,
    method: 'delete'
  })
}
