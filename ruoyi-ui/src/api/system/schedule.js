import request from '@/utils/request'

// 查询实验室排课列表
export function listSchedule(query) {
  return request({
    url: '/system/schedule/list',
    method: 'get',
    params: query
  })
}

// 查询实验室排课详细
export function getSchedule(id) {
  return request({
    url: '/system/schedule/' + id,
    method: 'get'
  })
}

// 新增实验室排课
export function addSchedule(data) {
  return request({
    url: '/system/schedule',
    method: 'post',
    data: data
  })
}

// 修改实验室排课
export function updateSchedule(data) {
  return request({
    url: '/system/schedule',
    method: 'put',
    data: data
  })
}

// 删除实验室排课
export function delSchedule(id) {
  return request({
    url: '/system/schedule/' + id,
    method: 'delete'
  })
}
