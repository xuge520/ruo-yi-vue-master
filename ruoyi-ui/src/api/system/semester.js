import request from '@/utils/request'

// 查询学期列表
export function listSemester(query) {
  return request({
    url: '/system/semester/list',
    method: 'get',
    params: query
  })
}

// 查询学期详细
export function getSemester(id) {
  return request({
    url: '/system/semester/' + id,
    method: 'get'
  })
}

// 新增学期
export function addSemester(data) {
  return request({
    url: '/system/semester',
    method: 'post',
    data: data
  })
}

// 修改学期
export function updateSemester(data) {
  return request({
    url: '/system/semester',
    method: 'put',
    data: data
  })
}

// 删除学期
export function delSemester(id) {
  return request({
    url: '/system/semester/' + id,
    method: 'delete'
  })
}
