-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室排课表', '3', '1', 'labscheduling', 'labscheduling/labscheduling/index', 1, 0, 'C', '0', '0', 'labscheduling:labscheduling:list', '#', 'admin', sysdate(), '', null, '实验室排课表菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室排课表查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'labscheduling:labscheduling:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室排课表新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'labscheduling:labscheduling:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室排课表修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'labscheduling:labscheduling:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室排课表删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'labscheduling:labscheduling:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('实验室排课表导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'labscheduling:labscheduling:export',       '#', 'admin', sysdate(), '', null, '');