/*
Navicat MySQL Data Transfer

Source Server         : localhost_3308
Source Server Version : 80018
Source Host           : localhost:3308
Source Database       : ry-vue

Target Server Type    : MYSQL
Target Server Version : 80018
File Encoding         : 65001

Date: 2024-04-27 21:39:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for labscheduling
-- ----------------------------
DROP TABLE IF EXISTS `labscheduling`;
CREATE TABLE `labscheduling` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `term` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lab_type` varchar(255) DEFAULT NULL,
  `lab_num` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `week_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `date_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `class_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `course_name` varchar(100) DEFAULT NULL,
  `instructor_name` varchar(100) DEFAULT NULL,
  `student_class` varchar(100) DEFAULT NULL,
  `start_week` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `end_week` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of labscheduling
-- ----------------------------
INSERT INTO `labscheduling` VALUES ('6', null, null, null, '1', '1', '1-2', '1', '1', '1', null, null);
INSERT INTO `labscheduling` VALUES ('7', '1', null, '1', '1', '1', '1-2', '1', '1', '1', null, null);
